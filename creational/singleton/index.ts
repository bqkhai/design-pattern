import { Singleton } from "./singleton";

export default class SingletonExample {
  public constructor() {
    this.run();
  }

  private run() {
    let s1 = Singleton.getInstance();
    console.log(s1);
  }
}

const test = () => {
  const s1 = new SingletonExample();
  console.log(s1);
}

test();
