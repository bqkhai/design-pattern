// Lazy Initialization
/**
 * Contructor private
 * Sử dụng static
 * public static method để các class bên ngoài có thể làm việc với singleton class.
 */

export class Singleton {
  private static instance: Singleton;

  // Contructor should always be private
  private constructor() {};

  // status method access singleton instance
  public static getInstance(): Singleton {
    if (!Singleton.instance) {
      Singleton.instance = new Singleton();
    }

    return Singleton.instance;
  }
}
