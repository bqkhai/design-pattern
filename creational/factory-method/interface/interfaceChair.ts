export type dimension = {
  height: number;
  width: number;
  depth: number;
}

export default interface IChair {
  height: number;
  width: number;
  depth: number;
  getDimensions() : dimension;
}
