## Factory method

![](2022-10-15-15-58-47.png)

- Factory Method là gì?
  + Factory Method Design Pattern hay thường được gọi là Factory Pattern là một trong những Pattern thuộc nhóm Creational Pattern. Nhiệm vụ của Factory Pattern là quản lý và trả về các đối tượng theo yêu cầu, giúp cho việc khởi tạo đổi tượng một cách linh hoạt hơn.

  + Nói 1 cách dễ hiểu thì Factory Pattern là một công xưởng và sẽ “sản xuất” các đối tượng theo yêu cầu của chúng ta. Phương pháp này được sử dụng khi chúng ta có 1 class mẹ (super-class) và nhiều class con (sub-class), sau đó dựa trên đầu vào chúng ta phải trả về một class con tương ứng.

- Sử dụng Factory Method như thế nào?
  + Factory Method được sử dụng khi chúng ta có 1 class mẹ và nhiều class con, sau đó chúng ta phải trả về kết quả 1 sub-class dựa trên đầu vào. Ngoài ra, Factory Method có thể được sử dụng trong trường hợp chúng ta không biết liệu tương lai có cần thêm sub-class nữa không. Do đó, khi cần mở rộng, chúng ta có thể tạo một 1 sub-class và sử dụng Factory Method để triển khai sub-class này.

- Factory Method bao gồm những phần nào?
  + Một Factory Pattern bao gồm các thành phần cơ bản sau:
    + Super Class: một supper class trong Factory Pattern có thể là một interface, abstract class hay một class thông thường
    + Sub Classes: mỗi sub class sẽ có nhiệm vụ riêng và chúng sẽ implement các phương thức của super class theo nhiệm vụ đó
    + Factory Class: một class chịu tránh nhiệm khởi tạo các đối tượng sub class dựa theo tham số đầu vào
