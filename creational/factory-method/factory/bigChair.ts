import IChair from "../interface/interfaceChair";

export default class BigChair implements IChair {
  height: number;
  width: number;
  depth: number;

  constructor() {
    this.height = 10;
    this.width = 10;
    this.depth = 10;
  }

  getDimensions() {
    return {
      height: this.height,
      width: this.width,
      depth: this.depth,
    };
  }
}
