import IChair from "../interface/interfaceChair";
import BigChair from "./bigChair";
import SmallChair from "./smallChair";

export default class ChairFactory {
  public static getChair(chair: string): IChair {
    if (chair == "BigChair") {
      return new BigChair();
    } else if (chair == "SmallChair") {
      return new SmallChair();
    }
    return null as any as IChair;
  }
}
