import IChair from "../interface/interfaceChair";

export default class SmallChair implements IChair {
  height: number;
  width: number;
  depth: number;

  constructor() {
    this.height = 1;
    this.width = 1;
    this.depth = 1;
  }

  getDimensions() {
    return {
      height: this.height,
      width: this.width,
      depth: this.depth,
    };
  }
}
