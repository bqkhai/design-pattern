## Builder

![](2022-10-14-14-33-46.png)

The classes that make up this pattern are the following:

- Product is the concrete result of a construction process. That is, they will be the models of our application.

- Builder is a common interface for the concrete builders.

- ConcreteBuilder are different implementations of the constructive process. These classes will be responsible for clarifying the differences in the business logic of each of the object construction processes.

    + These classes will be responsible for clarifying the differences between the business logic of each of the object construction processes.

- Director defines the order in which the construction steps are performed. Its purpose is the reusability of specific configurations. The Director can be omitted in some implementations of this pattern, although its use is highly recommended, since it abstracts the client from the concrete steps of construction to the client.

- Client is the class that uses the pattern. There are two possibilities:

    + The client uses the ConcreteBuilder, executing the construction steps one by one.

    + The client uses the Director which implements each of the construction processes, and acts as an intermediary between the Client and the ConcreteBuilder classes.

![](2022-10-14-14-33-12.png)

