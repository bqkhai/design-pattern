import Product from "./product";
 
export default interface IBuilder {
  buildPartA(): this;
  buildPartB(): this;
  buildPartC(): this;
  getResult(): Product;
}

export default class Builder implements IBuilder {
  private product: Product;

  constructor(){
    this.product = new Product();
  }

  buildPartA() {
    this.product.parts.push('a');
    return this;
  }

  buildPartB() {
    this.product.parts.push('b');
    return this;
  }

  buildPartC() {
    this.product.parts.push('c');
    return this;
  }

  getResult(): Product {
    return this.product;
  }
}
