import Builder from "./builder";

export default class Director {
  public static construct() {
    return new Builder()
      .buildPartA()
      .buildPartB()
      .buildPartC()
      .getResult();
  }
}
