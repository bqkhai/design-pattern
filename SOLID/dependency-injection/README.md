## Lợi ích khi dùng Dependency Injection:
  - Dễ test và viết Unit Test: Dễ hiểu là khi ta có thể Inject các dependency vào trong một class thì ta cũng dễ dàng “tiêm” các mock object vào class (được test) đó.
  - Dễ dàng thấy quan hệ giữa các object: Dependency Injection sẽ inject các object phụ thuộc vào các interface thành phần của object bị phụ thuộc nên ta dễ dàng thấy được các dependency của một object.
  - Dễ dàng hơn trong việc mở rộng các ứng dụng hay tính năng.
  - Giảm sự kết dính giữa các thành phần, tránh ảnh hưởng quá nhiều khi có thay đổi nào đó.
## Bất lợi của Dependency Injection:
  - Nó khá là phức tạp để học, và nếu dùng quá đà thì có thể dẫn tới một số vấn đề khác.
  - Rất nhiều các lỗi ở compile time có thể bị đẩy sang runtime, dẫn đến đôi khi sẽ khó debug. Vì sử dụng các Interface nên có thể gặp khó khăn khi ta debug source code vì không biết implement nào thực sự được truyền vào.
  - Có thể làm ảnh hưởng tới chức năng auto-complete hay find references của một số IDE. Cụ thể vì Dependency Injection ẩn các dependency nên một số lỗi chỉ xảy ra khi chạy chương trình thay vì có thể phát hiện khi biên dịch chương trình.
  - Khó khăn lớn nhất là khi người mới vào làm bằng DI sẽ không hiểu rõ ràng tư tưởng, khiến quá trình làm DI vẫn bị nhập nhằng và các injector bị ràng buộc mà không thoát hẳn ra theo tư tưởng của DI.
