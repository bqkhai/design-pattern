## SOLID

• S: Single responsibility priciple: Mỗi lớp chỉ nên chịu trách nhiệm về một nhiệm vụ
cụ thể nào đó mà thôi.

• O: Open/Closed principle: Không được sửa đổi một Class có sẵn, nhưng có thể mở
rộng bằng kế thừa.

• L: Liskov substitution principe: Các đối tượng (instance) kiểu class con có thể thay
thế các đối tượng kiểu class cha mà không gây ra lỗi.

• I: Interface segregation principle: Thay vì dùng 1 interface lớn, ta nên tách thành
nhiều interface nhỏ, với nhiều mục đích cụ thể.

• D: Dependency inversion principle
  o Các module cấp cao không nên phụ thuộc vào các modules cấp thấp. Cả 2 nên
  phụ thuộc vào abstraction.
  o Interface (abstraction) không nên phụ thuộc vào chi tiết, mà ngược lại (Các class
  giao tiếp với nhau thông qua interface (abstraction), không phải thông qua
  implementation.)
